package com.apache;

import java.io.Serializable;

@SuppressWarnings("serial")
public class 	SysConfig implements Serializable {
	private String uuid;
	/**
	 * 系统编码
	 */
	private String sysId;
	/**
	 * 系统名称
	 */
	private String sysName;
	/**
	 * 配置文件名称
	 */
	private String fileName;
	/**
	 * 配置文件内容
	 */
	private String fileContent;
	/**
	 * 模式:开发 运行
	 */
	private String model;

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getSysId() {
		return sysId;
	}

	public void setSysId(String sysId) {
		this.sysId = sysId;
	}

	public String getSysName() {
		return sysName;
	}

	public void setSysName(String sysName) {
		this.sysName = sysName;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFileContent() {
		return fileContent;
	}

	public void setFileContent(String fileContent) {
		
		
		this.fileContent = fileContent;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

}
