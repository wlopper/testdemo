package com.apache;

import com.css.app.sysconfig.model.SysConfig;
import com.css.core.configuration.Environment;
import com.css.db.query.QueryCache;
import com.css.util.Messages;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import net.sf.json.JSONArray;

import org.slw.common.utils.Ajax;

import java.util.HashMap;
import java.util.List;

/**
 * @author shangdu
 * @version 1.0
 * @date 2020/5/12 10:50
 */
public class GetSConfigTree {
	
    public JsonArray execute() {
        JsonArray ary = new JsonArray();
        //QueryCache qc = new QueryCache("select distinct a.sysId,a.uuid,a.sysName from SysConfig a where a.sysId in ( select a.sysId from SysConfig a group by a.sysId )");
        QueryCache qc = new QueryCache("select a.uuid from SysConfig a where a.uuid in (select max(a.uuid) from SysConfig a group by a.sysId)");
        List<SysConfig> resList = QueryCache.idToObj(SysConfig.class,qc.list());
        for (SysConfig res:
             resList) {
            JsonObject obj = new JsonObject();
            obj.addProperty("id",res.getSysId());
            obj.addProperty("name",res.getSysName());
            obj.addProperty("pId","0");
            obj.addProperty("isParent",false);
            ary.add(obj);
        }
        return ary;
    }
}
